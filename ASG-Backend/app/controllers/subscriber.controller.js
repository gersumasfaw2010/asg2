const db = require("../models");
const SubscriberM = db.subscribers;
const Farmlocation = db.farmlocations;

// Create and Save a new Subscriber
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a Subscriber
  const subscriber = new SubscriberM({
    ...req.body, owner: req.userId, farmlocation: req.body.farmlocationId
  });

  // Save Subscriber in the database
  subscriber
    .save(subscriber)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Subscriber."
      });
    });
};




// Retrieve all Subscribers from the database.
exports.findAll = async(req, res) => {
  // const name = req.query.name;
  // var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  const subscriber = await SubscriberM.find({ owner: req.userId })
  .populate({ path:'farmlocations', select: 'longitude'}).populate({ path:'farmlocations', select: 'longitude latitude -_id'}).exec((err, subscribers) => {
    if (err)
      res.status(400).send(err);
    else
      res.status(200).send(subscribers)
      
     

  })

 
  // .then(data => {
  //   res.send(data);
  // })
  // .catch(err => {
  //   res.status(500).send({
  //     message:
  //       err.message || "Some error occurred while retrieving subscribers."
  //   });
  // });
};

// Find a single Subscriber with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  SubscriberM.findById(id).populate('farmlocations')
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found Subscriber with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Subscriber with id=" + id });
    });
};

// Update a Subscriber by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  SubscriberM.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Subscriber with id=${id}. Maybe Subscriber was not found!`
        });
      } else res.send({ message: "Subscriber was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Subscriber with id=" + id
      });
    });
};



// Update a Subscriber by the id in the request
// exports.updateSubscriberwithLocation = (req, res) => {
//   if (!req.body) {
//     return res.status(400).send({
//       message: "Data to update can not be empty!"
//     });
//   }
//   const location = req.body;
//   const subId = req.params.id;
//   //const farmlocation = req.params.farmlocation
//   const newLocation = Farmlocation.create(location)

//   Subscriber.findByIdAndUpdate(
//     subId,
//     {
//       $push:{

//            ownerlocations:newLocation._id
//       }
//     },
//     {new:true,userFindAndModify:false},
//     )
//     .then(data => {
//       if (!data) {
//         res.status(404).send({
//           message: `Cannot update Subscriber with id=${subId}. Maybe Subscriber was not found!`
//         });
//       } else res.send({ message: "Subscriber was updated successfully." });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Error updating Subscriber with id=" + subId
//       });
//     });
// };


//get subscribers farmlocation informaiton 
exports.getSublocation = async (req, res, next) => {
  const subId = req.params.id;
  const subscriber = await SubscriberM.findById(subId).populate('farmlocations').exec((err, subscribers) => {
    
    if (err)
      res.status(400).send(err);
    else
      res.status(200).send(subscribers)
      // for (let i = 0; i < subscribers.farmlocations.length; i++){
      // console.log(subscribers.farmlocations[i].longitude);
      // console.log(subscribers.farmlocations[i].latitude);
  // }

  })
 // console.log(subscriber.farmlocations[0].longitude);
}





exports.newSublocation = async (req, res, next) => {
  const subId = req.params.id;

  // create new location
  const newLocation = new Farmlocation(req.body);  //console.log('newLocation',newLocation);

  //Get a subscriber
  const Subscriber = await SubscriberM.findById(subId)
  //assign subscriber as a farmfield owner
  newLocation.farmers = Subscriber;
  // save the car
  await newLocation.save();

  //Add location to the subscribers's location array 'location'

  Subscriber.farmlocations.push(newLocation);
  // save the subscriber
  //console.log('newLocation',newLocation);

  await Subscriber.save();
 res.status(201).json(newLocation);
}








// Delete a Subscriber with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  SubscriberM.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Subscriber with id=${id}. Maybe Subscriber was not found!`
        });
      } else {
        res.send({
          message: "Subscriber was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Subscriber with id=" + id
      });
    });
};

// Delete all Subscribers from the database.
exports.deleteAll = (req, res) => {
  SubscriberM.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Subscribers were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all subscribers."
      });
    });
};

// Find all published Subscribers
exports.findAllPublished = (req, res) => {
  SubscriberM.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving subscribers."
      });
    });
};

// allcount
exports.subscriberCount = (rep,res,next) =>{
    
  SubscriberM.aggregate([
    {
      $group: {
        _id:1,
        count:{
          $sum:1
        }
      }
    }
   
  ],
  (error,data)=>{
    if (error){
      return next(error);
    } else {
      res.json(data);
    }

  }
  )

}

//specific subscriber count 

exports.subscriberSpecificCount =  (req,res,next) =>{
  SubscriberM.find({ owner: req.userId })
  .exec((err, subscribers) => {
   let count = subscribers.length
   res.status(200).send((count).toString());
 })
};
  