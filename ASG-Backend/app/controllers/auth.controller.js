const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;
const Subscribers = db.subscribers;
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    //roles:req.body.roles
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (req.body.roles) {
      Role.find(
        {
          name: { $in: req.body.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          user.roles = roles.map(role => role.id);
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }

            res.send({ message: "User was registered successfully!" });
          });
        }
      );
    } else {
      Role.findOne({ name: "user" }, (err, role) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }

        user.roles = [role._id];
        user.save(err => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }

          res.send({ message: "User was registered successfully!" });
        });
      });
    }
  });
};

exports.userCount = (rep,res,next) =>{
    
     User.aggregate([
       {
         $group: {
           _id:1,
           count:{
             $sum:1
           }
         }
       }
      
     ],
     (error,data)=>{
       if (error){
         return next(error);
       } else {
         res.json(data);
       }

     }
     )
  
}
exports.getAll  = async (req, res) => {
 try 
 {
  const users = await User.find({}).populate('roles')
  .exec()
   if (!users)
   return res.status(404).send()
   res.status(200).send(users)
   //console.log(users);
 }

 catch (e){
   res.status(500).send(e.message)

    }
 };



exports.getById  = async (req, res) => {
  try 
  {
   const user = await User.findById(req.params.id)
   .populate('roles')
   //.exec((err,role)=>{console.log(role);})
     res.status(200).send(user)
   
    if (!user)
    return res.status(404).send()
    //res.status(200).send(user)
    //console.log(user);
  }
 
  catch (e){
    res.status(500).send(e.message)
 
  }
  
   
 };


 exports.userSubscribersCount = (rep,res,next) =>{
    
  User.aggregate([
    {
      "$project": {
          "_id": 1,
          "Subscriber": 1
      }
   },

   {
    "$unwind": "$Subscriber"
 },

 {
  "$group": {
      "_id": '$Subscriber',
      "count": {"$sum": 1}
      
  }

 
},
{"$sort": {"_id": -1}},
{"$limit": 5}
   ], (error,data)=>{
     if (error){
       return next(error);
     } else {
       res.json(data);
      // console.log(data[1].count);
     }
   
   }
   )
   


}



exports.userRoleCount = (rep,res,next) =>{
    
  User.aggregate([
    {
      "$project": {
          "_id": 1,
          "roles": 1
      }
   },

   {
    "$unwind": "$roles"
 },

 {
  "$group": {
      "_id": '$roles',
      "count": {"$sum": 1}
  }
},
{"$sort": {"_id": -1}},
{"$limit": 5}
   ], (error,data)=>{
     if (error){
       return next(error);
     } else {
       res.json(data);
      // console.log(data[1].count);
     }
   
   }
   )
   


}

//// backup

// exports.userRoleCount = (rep,res,next) =>{
    
//   User.aggregate([
//     {
//       "$project": {
//           "_id": 1,
//           "roles": 1
//       }
//    },

//    {
//     "$unwind": "$roles"
//  },

//  {
//   "$group": {
//       "_id": '$roles',
//       "count": {"$sum": 1}
//   }
// },
    
//    ], (error,data)=>{
//      if (error){
//        return next(error);
//      } else {
//        res.json(data);
//      }
   
//    }
//    )
   


// }

//  exports.editById  = async (req, res) =>{
//   try 
//   {
//    const user = await User.findById(req.params.id,req.body,{new : true, runValidators:true})
//     if (!user)
//     return res.status(404).send()
//     res.status(200).send(user)
//     console.log(user);
//   } 
 
//   catch (e){
//     res.status(500).send(e.message)
 
//   }
  
   
//  };

//////////////////////////////////////////////////

//  exports.editById = async (req,res)=>{
//   try {
//    const updatedPost = await User.updateMany({id : req.params.id},
//        {$set : {
//                username: req.body.username, 
//                email : req.body.email,
//                password: req.body.password,
//                //roles: req.body.roles,
//                }});
//        res.json(updatedPost);

//   } catch (err) {
//        res.json({message : err})
//   }
// };
// // exports.editById = async (req,res)=>{
// // const id = req.params.id;

// //   User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
// //     .then(data => {
// //       if (!data) {
// //         res.status(404).send({
// //           message: `Cannot update User with id=${id}. Maybe User was not found!`
// //         });
// //       } else res.send({ message: "User was updated successfully." });
// //     })
// //     .catch(err => {
// //       res.status(500).send({
// //         message: "Error updating user with id=" + id
// //       });
// //     });

// //   };


//   // Delete a user by specific ID
//   exports.delete = (req, res) => {
//     const id = req.params.id;
  
//     User.findByIdAndRemove(id, { useFindAndModify: false })
//       .then(data => {
//         if (!data) {
//           res.status(404).send({
//             message: `Cannot delete User with id=${id}. Maybe Log was not found!`
//           });
//         } else {
//           res.send({
//             message: "User was deleted successfully!"
//           });
//         }
//       })
//       .catch(err => {
//         res.status(500).send({
//           message: "Could not delete User with id=" + id
//         });
//       });
//   };


//   // Delete all Users from the database.
// exports.deleteAll = (req, res) => {
//   User.deleteMany({})
//     .then(data => {
//       res.send({
//         message: `${data.deletedCount} Users were deleted successfully!`
//       });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while removing all logs."
//       });
//     });
// };




// Login a User

exports.signin = (req, res) => {
  User.findOne({
    username: req.body.username
  })
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      var authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      res.status(200).send({
        id: user.id,
        username: user.username,
        email: user.email,
        roles: authorities,
        accessToken: token
      });
    });
};

//////////////////////////////////////////////////////////////////////////////

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findById(id).populate('roles').exec()
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found User with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving User with id=" + id });
    });
};

// Update a User by the id in the request
exports.editById = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update User with id=${id}. Maybe User was not found!`
        });
      } else res.send({ message: "User was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  User.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      } else {
        res.send({
          message: "User was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  User.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Users were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all users."
      });
    });
};

// Find all published Users
exports.findAllPublished = (req, res) => {
  User.find({ published: true })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};
